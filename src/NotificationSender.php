<?php
namespace Humanity\Notifications;
use Monolog\Logger;

/**
 * Class NotificationSender
 *
 * @package Humanity\Notifications
 * @author Ruben Knol <ruben@turnware.rs>
 */
abstract class NotificationSender
{
	/**
	 * @var NotificationSenderAdapterInterface $notificationSenderAdapter
	 */
	protected $notificationSenderAdapter;

	/**
	 * Set the notification sender adapter associated with the impl class
	 * @constructor
	 */
	public function __construct()
	{
		$this->notificationSenderAdapter = $this->setNotificationSenderAdapter();
	}

	/**
	 * Retrieve the associated notification sender adapter
	 * @return NotificationSenderAdapterInterface
	 */
	protected function getNotificationSenderAdapter()
	{
		return $this->notificationSenderAdapter;
	}

	/**
	 * Register the notification sender adapter
	 * @return NotificationSenderAdapterInterface
	 */
	abstract function setNotificationSenderAdapter();

	/**
	 * Send the notification to the Actor
	 *
	 * @param ActorInterface $actor
	 * @param NotificationInterface $notification
	 * @param Logger $logger
	 * @return void
	 */
	abstract function send(ActorInterface $actor, NotificationInterface $notification, Logger $logger);
}