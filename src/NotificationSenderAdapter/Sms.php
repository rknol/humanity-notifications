<?php
namespace Humanity\Notifications\NotificationSenderAdapter;

use Humanity\Notifications\NotificationSenderAdapterInterface;
use Monolog\Logger;

/**
 * Class Sms
 *
 * @package Humanity\Notifications\NotificationSenderAdapter
 * @author Ruben Knol <ruben@turnware.rs>
 */
class Sms implements NotificationSenderAdapterInterface
{
	/**
	 * @var string
	 */
	protected $phoneNumber;

	/**
	 * @var string
	 */
	protected $message;

	/**
	 * @return string
	 */
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}

	/**
	 * @param string $phoneNumber
	 */
	public function setPhoneNumber($phoneNumber)
	{
		$this->phoneNumber = $phoneNumber;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @param Logger $logger
	 */
	public function send(Logger $logger)
	{
		$logger->info('Dispatching SMS message', [
			'phoneNumber' => $this->getPhoneNumber(),
			'message' => $this->getMessage()
		]);
	}
}