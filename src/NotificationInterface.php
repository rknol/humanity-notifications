<?php
namespace Humanity\Notifications;

/**
 * Interface NotificationInterface
 *
 * @package Humanity\Notifications
 * @author Ruben Knol <ruben@turnware.rs>
 */
interface NotificationInterface
{
	/**
	 * Register all NotificationSender objects associated with a
	 * specific notification
	 *
	 * @return NotificationSender[]
	 */
	public function registerNotificationSenders();
}