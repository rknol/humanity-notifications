<?php
namespace Humanity\Notifications;

/**
 * Interface ActorInterface
 *
 * @package Humanity\Notifications
 * @author Ruben Knol <ruben@turnware.rs>
 */
interface ActorInterface
{
	/**
	 * Get the name of the Actor
	 * @return string
	 */
	public function getName();

	/**
	 * Get the e-mail address of the Actor
	 * @return string
	 */
	public function getEmailAddress();

	/**
	 * Get the phone number of the Actor
	 * @return string
	 */
	public function getPhoneNumber();
}