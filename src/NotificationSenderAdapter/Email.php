<?php
namespace Humanity\Notifications\NotificationSenderAdapter;

use Humanity\Notifications\NotificationSenderAdapterInterface;
use Monolog\Logger;

/**
 * Class Email
 *
 * @package Humanity\Notifications\NotificationSenderAdapter
 * @author Ruben Knol <ruben@turnware.rs>
 */
class Email implements NotificationSenderAdapterInterface
{
	/**
	 * @var string
	 */
	protected $to;

	/**
	 * @var string
	 */
	protected $subject;

	/**
	 * @var string
	 */
	protected $body;

	/**
	 * @return string
	 */
	public function getTo()
	{
		return $this->to;
	}

	/**
	 * @param string $to
	 */
	public function setTo($to)
	{
		$this->to = $to;
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * @param string $body
	 */
	public function setBody($body)
	{
		$this->body = $body;
	}

	/**
	 * Send the notification
	 * @param Logger $logger
	 */
	public function send(Logger $logger)
	{
		$logger->addInfo('Dispatching e-mail', [
			'to' => $this->getTo(),
			'subject' => $this->getSubject(),
			'body' => $this->getBody()
		]);
	}
}