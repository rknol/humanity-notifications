<?php
namespace Humanity\Notifications;
use Monolog\Logger;

/**
 * Class NotificationManager
 *
 * @package Humanity\Notifications
 * @author Ruben Knol <ruben@turnware.rs>
 */
class NotificationManager
{
	/**
	 * @var Logger
	 */
	protected $logger;

	/**
	 * Instantiate a NotificationManager
	 * @param Logger $logger
	 * @constructor
	 */
	public function __construct(Logger $logger = NULL)
	{
		$this->logger = $logger;
	}

	/**
	 * @return Logger
	 */
	public function getLogger()
	{
		return $this->logger;
	}

	/**
	 * Send a notification to an Actor through all available
	 * notification senders associated with the notification
	 *
	 * @param ActorInterface $actor
	 * @param NotificationInterface $notification
	 */
	public function send(ActorInterface $actor, NotificationInterface $notification)
	{
		$senders = $notification->registerNotificationSenders();
		foreach($senders as $sender)
		{
			$sender->send($actor, $notification, $this->getLogger());
		}
	}
}