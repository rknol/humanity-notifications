<?php
namespace Humanity\Notifications;
use Monolog\Logger;

/**
 * Interface NotificationSenderAdapterInterface
 *
 * @package Humanity\Notifications
 * @author Ruben Knol <ruben@turnware.rs>
 */
interface NotificationSenderAdapterInterface
{
	/**
	 * Send the notification
	 * @param Logger $logger
	 * @return void
	 */
	public function send(Logger $logger);
}